﻿using UnityEngine;

public class ExplosionFade : MonoBehaviour
{
    public GameObject explosion;
    void Update()
    {
        Destroy(explosion, 1f);
    }
}
