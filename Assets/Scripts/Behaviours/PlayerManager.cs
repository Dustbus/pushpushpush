﻿using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Singleton

    public static PlayerManager instance;

    void Awake()
    {
        instance = this;
}
    #endregion
    public GameObject player;
    public static int playerHP = 3;
    public static int playerMaxHP = 3;
    public static int playerEXP = 0;
    public static Vector3 GetPlayerProximity(float radius)
    {
        var vector2 = Random.insideUnitCircle.normalized * radius;
        return new Vector3(vector2.x, 0, vector2.y);
    }

    public static Vector3 playerProximity;
    void Update()
    {
        Mathf.Clamp(playerHP, 0, playerMaxHP);
        playerProximity = GetPlayerProximity(2.5f) + player.transform.position;
    }
}
