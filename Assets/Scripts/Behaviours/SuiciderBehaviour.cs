﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SuiciderBehaviour : MonoBehaviour
{
    public Collider suiciderCollider;
    public GameObject explosionPlaceholder;
    public GameObject suiciderObject;
    public Renderer suiciderModel;
    Transform target;
    NavMeshAgent navMeshAgent;

    public float aggroRange = 10f;
    bool isAggroed = false;
    public float rotationSpeed = 5f;
    public int hp = 2;
    public float despawnDelay = 0.5f;
    public static bool isAlive = true;
    private bool expAwarded = false;
    public Material dead;

    void Start()
    {
        target = PlayerManager.instance.player.transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (isAggroed == true)
        {
            aggroRange = 50f;
        }

        float distance = Vector3.Distance(target.position, transform.position);

        if(distance <= aggroRange)
        {
            //Debug.Log("Spotted");
            navMeshAgent.SetDestination(target.position);
        }

        #region Hit points
        if (hp <= 0)
        {
            isAlive = false;
            suiciderModel.material = dead;
            navMeshAgent.isStopped = true;
            Destroy(suiciderObject, despawnDelay);
            if (!expAwarded)
                PlayerManager.playerEXP += 100;
            Debug.Log("EXP: " + PlayerManager.playerEXP);
            expAwarded = true;
            if(MagicBullet.magicBulletEnabled) MagicBullet.MagicBulletEffect();
            if(Bloodlust.activated) StartCoroutine(Bloodlust.BloodlustBoost());
        }
        #endregion

        #region Facing player
        if (distance <= aggroRange && isAlive == true)
        {
            //Debug.Log("Spotted");
            isAggroed = true;
            FaceTarget();
        #endregion
        }
    }

    void OnCollisionEnter(Collision colissionInfo)
    {
        #region Receiving Damage
        //Debug.Log("Collision detected");
        if (colissionInfo.collider.tag == "ProjectilePlayer")
        {
            isAggroed = true;
            hp -= 1;
            //Debug.Log(hp);
        }
        #endregion
    }

    //detonating in proximity
    private void OnTriggerEnter(Collider collisionInfo)
    { 
        if (collisionInfo.tag == "Player" && isAlive == true)
        {
            Debug.Log("Triggered");
            navMeshAgent.isStopped = true;
            StartCoroutine(ExecuteAfterTime(1));
        }
    }

    IEnumerator ExecuteAfterTime(float delay)
    {
        yield return new WaitForSeconds(delay);

        Instantiate(explosionPlaceholder, transform.position, transform.rotation);
        Debug.Log("Explosion spawned");
        Destroy(suiciderObject);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, aggroRange);
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
    }
}