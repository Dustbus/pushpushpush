﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SocialPlatforms;

public class GunnerBehaviour : MonoBehaviour
{
    #region Basic Enemy Behaviour
    public GameObject enemyObject;
    public Renderer enemyModel;
    public Material dead;
    Transform target;
    NavMeshAgent navMeshAgent;
    public float aggroRange = 10f;
    public float movementRange = 30f;
    bool isAggroed = false;
    public float rotationSpeed = 1000f;
    //public Transform enemyTransform;
    public int hp = 2;
    public float despawnDelay = 0.5f;
    public static bool isAlive = true;
    private bool expAwarded = false;
    public GameObject expBubble;
    #region ShootingVariables
    public Transform firePoint;
    public GameObject bulletPrefab;
    public float bulletForce = 27f;
    public float fireRate = 60f; 
    private float nextShot = 0f;
    private bool lineOfSight = false;
    private float seekingDelay = 0f;
    //public Vector3 destination;
    #endregion

    void Start()
    {
        target = PlayerManager.instance.player.transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void OnCollisionEnter(Collision colissionInfo)
    {

        //Debug.Log("Collision detected");
        if (colissionInfo.collider.tag == "ProjectilePlayer")
        {
            isAggroed = true;
            hp -= 1;
            //Debug.Log(hp);
        }
    }

    void Update()
    {
        if(isAggroed == true)
        {
            aggroRange = 50f;
            if (Time.time > seekingDelay)
            {
                SeekDestination();
            }
        }

        #region Hit points
        if (hp <= 0)
        {
            isAlive = false;
            navMeshAgent.isStopped = true;
            enemyModel.material = dead;
            Destroy(enemyObject, despawnDelay);
            if (!expAwarded)
            {
                Debug.Log("EXP awarded");
                for (int i = 2; i >= 0; i--)
                {
                    //Debug.Log("Bubble spawned");
                    Vector3 positionOffset = transform.position + new Vector3(i * .5f, 0, i * .5f);
                    Instantiate(expBubble, positionOffset, transform.rotation);
                }
                /*PlayerManager.playerEXP += 100;
                Debug.Log("EXP: " + PlayerManager.playerEXP);*/
            }
            expAwarded = true;
        }
        #endregion

        #region Facing player
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= aggroRange)
        {
            //Debug.Log("GunnerBehaviour: Spotted");
            isAggroed = true;
            FaceTarget();
        }

        void FaceTarget()
        {
            Vector3 direction = (target.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed); //strzelanie z "wcelowaniem" 
            //transform.rotation = lookRotation; //ciągłe strzelanie
        }
        #endregion

        RaycastHit hitInfo;
        if(Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out hitInfo, aggroRange))
        {
            if(hitInfo.transform.tag == "Player")
            {
                lineOfSight = true;
            }
            else
            {
                lineOfSight = false;
            }
        }

        if (Time.time > nextShot && distance <= aggroRange && lineOfSight == true)
        {
            Shoot();
            //Debug.Log("Shot");
        }
    }
    #endregion

    void Shoot()
    {
        nextShot = Time.time + fireRate;
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        bullet.transform.Rotate(Random.Range(-5, 5), Random.Range(-5, 5), Random.Range(-5, 5));
        rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);
    }

    void SeekDestination()
    {
        seekingDelay = Time.time + 1.5f;
        //Debug.Log("GunnerBehaviour: Seeking new destination");
        /*Vector3 randomDirection = Random.insideUnitSphere * movementRange;

        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, movementRange, 1);
        Vector3 finalPosition = hit.position;

        navMeshAgent.SetDestination(finalPosition);*/
   
        navMeshAgent.SetDestination(PlayerManager.playerProximity);
    }

}
