﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Template_BasicEnemyBehaviour : MonoBehaviour
{
    #region Basic Enemy Behaviour
    public GameObject enemyObject;
    Transform target;
    NavMeshAgent navMeshAgent;
    public float aggroRange = 10f;
    public float rotationSpeed = 5f;
    //public Transform enemyTransform;
    public int hp = 2;
    public float despawnDelay = 0.5f;
    public static bool isAlive = true;

    void Start()
    {
        target = PlayerManager.instance.player.transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void OnCollisionEnter(Collision colissionInfo)
    {

        //Debug.Log("Collision detected");
        if (colissionInfo.collider.tag == "ProjectilePlayer")
        {
            hp -= 1;
            //Debug.Log(hp);
        }
    }

    void Update()
    {
        #region Hit points
        if (hp <= 0)
        {
            isAlive = false;
            navMeshAgent.isStopped = true;
            enemyObject.transform.Rotate(90.0f, 0.0f, 0.0f, Space.Self);
            Destroy(enemyObject, despawnDelay);
        }
        #endregion

        #region Facing player
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= aggroRange)
        {
            //Debug.Log("Spotted");
            FaceTarget();
        }

        void FaceTarget()
        {
            Vector3 direction = (target.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed); // nie wiem, czy wogóle dawać im opóźnienie, bo wtedy nie nadążają gdy się biega wokół nich 
        }
        #endregion
    }
    #endregion
}
