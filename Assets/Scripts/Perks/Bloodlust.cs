﻿using UnityEngine;
using System.Collections;

//Speeds up reload for 1 second after killing a enemy.

public class Bloodlust : MonoBehaviour
{
    public static bool activated = false;
    void Update()
    {
        if(gameObject.GetComponent<Bloodlust>().enabled && !activated)
        {
            activated = true;
        }
    }
    public static IEnumerator BloodlustBoost()
    {
        ShootingAR.reloadTime /= 2;
        ShootingBolt.reloadTime /= 2;
        ShootingFlak.reloadTime /= 2;
        ShootingPlasma.reloadTime /= 2;
        ShootingShotgun.reloadTime /= 2;
        ShootingSSG.reloadTime /= 2;
        ShootingStarter.reloadTime /= 3;
        Movement.speed = 700;
        yield return new WaitForSeconds(0.7f); 
        ShootingAR.reloadTime *= 2;
        ShootingBolt.reloadTime *= 2;
        ShootingFlak.reloadTime *= 2;
        ShootingPlasma.reloadTime *= 2;
        ShootingShotgun.reloadTime *= 2;
        ShootingSSG.reloadTime *= 2;
        ShootingStarter.reloadTime *= 3;
        Movement.speed = 650;
    }
}
        
