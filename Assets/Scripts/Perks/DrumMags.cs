﻿using UnityEngine;

public class DrumMags : MonoBehaviour
{
    bool activated = false;
    void Update()
    {
        if(gameObject.GetComponent<DrumMags>().enabled && !activated)
        {
            ShootingAR.magSize = 30;
            ShootingBolt.magSize = 8;
            ShootingFlak.magSize = 6;
            ShootingPlasma.magSize = 50;
            ShootingShotgun.magSize = 9;
            ShootingStarter.magSize = 20;
            ShootingSSG.magSize = 3;
            activated = true;
        }
    }
}
