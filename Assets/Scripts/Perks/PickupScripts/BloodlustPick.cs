﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodlustPick : MonoBehaviour
{
    public Transform player;
    public GameObject perkMenager;
    public GameObject icon;
    bool triggerCheck = false;

    private void Start()
    {
        StartCoroutine(FindPlayerAndPerkMenager());
    }
    private void OnTriggerEnter(Collider colider)
    {
        if (colider.tag == "Player")
        {
            triggerCheck = true;
            icon.GetComponent<MeshRenderer>().enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            triggerCheck = false;
            icon.GetComponent<MeshRenderer>().enabled = false;
        }
    }


    private void Update()
    {
        if (triggerCheck)
        {
            if (Input.GetKeyDown("e"))
            {
                Pickup();
            }
        }
    }
    void Pickup()
    {
        perkMenager.GetComponent<Bloodlust>().enabled = true;
        Destroy(GameObject.FindWithTag("PerkPickup"));
    }
    IEnumerator FindPlayerAndPerkMenager()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
        GameObject[] menager = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPerkMenager in menager)
        {
            perkMenager = foundPerkMenager;
        }
    }
}