﻿using UnityEngine;
using System.Collections;

public class PerkSpawnPointController : MonoBehaviour
{
    public GameObject[] perkSpawns;
    public Transform spawnPoint;
    public GameObject perkMenager;

    GameObject perkPickup;
    void Start()
    {
        StartCoroutine(FindPerkMenager());

        perkPickup = perkSpawns[Random.Range(0, perkSpawns.Length)];

        Instantiate(perkPickup, spawnPoint);
    }
    IEnumerator FindPerkMenager()
    {
        yield return new WaitForSeconds(0.01f);
        GameObject[] menager = GameObject.FindGameObjectsWithTag("PerkMenager");
        foreach (GameObject foundPerkMenager in menager)
        {
            perkMenager = foundPerkMenager;
        }
    }
}
