﻿using UnityEngine;
using System.Collections;

public class DragonEntranceP : MonoBehaviour
{
    public Transform player;
    public GameObject perkMenager;
    public GameObject icon;
    bool triggerCheck = false;

    private void Start()
    {
        StartCoroutine(FindPlayerAndPerkMenager());

    }
    private void OnTriggerEnter(Collider colider)
    {
        if (colider.tag is "Player")
        {
            triggerCheck = true;
            icon.GetComponent<MeshRenderer>().enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag is "Player")
        {
            triggerCheck = false;
            icon.GetComponent<MeshRenderer>().enabled = false;
        }
    }


    private void Update()
    {
        if (triggerCheck)
        {
            if(Input.GetKeyDown("e"))
            {
                Pickup();
            }
        }
    }
    void Pickup()
    {
        perkMenager.GetComponent<DragonEntrance>().enabled = true;
        GameObject[] perkPickups = GameObject.FindGameObjectsWithTag("PerkPickup");
        foreach (GameObject perkPickup in perkPickups)
            GameObject.Destroy(perkPickup);
    }
    IEnumerator FindPlayerAndPerkMenager()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
        GameObject[] menager = GameObject.FindGameObjectsWithTag("PerkMenager");
        foreach (GameObject foundPerkMenager in menager)
        {
            perkMenager = foundPerkMenager;
        }
    }
}
