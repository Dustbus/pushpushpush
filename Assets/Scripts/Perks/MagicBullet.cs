﻿using UnityEngine;

public class MagicBullet : MonoBehaviour
{
    public static bool magicBulletEnabled = false;
    void Update()
    {
        if (gameObject.GetComponent<MagicBullet>().enabled && !magicBulletEnabled)
        {
            magicBulletEnabled = true;
        }
    }
    public static void MagicBulletEffect()
    {
        if (MagicBullet.magicBulletEnabled)
        {
            ShootingAR.bulletsInMag++;
            ShootingBolt.bulletsInMag++;
            ShootingFlamer.ammo += 2;
            ShootingPlasma.bulletsInMag++;
            ShootingShotgun.bulletsInMag++;
            ShootingStarter.bulletsInMag += 2;
            if (ShootingFlak.bulletsInMag < ShootingFlak.magSize)
            {
                ShootingFlak.bulletsInMag++;
                ShootingFlak.ammo--;
            }
            if(ShootingSSG.bulletsInMag < ShootingSSG.magSize)
            {
                ShootingSSG.bulletsInMag++;
                ShootingSSG.ammo--;
            }
        }
    }
}
