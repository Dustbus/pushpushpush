﻿using UnityEngine;

public class HealthPlus : MonoBehaviour
{
    bool activated = false;
    void Update()
    {
        if(gameObject.GetComponent<HealthPlus>().enabled && !activated)
        {
            PlayerManager.playerMaxHP += 2;
            activated = true;
        }
    }
}
