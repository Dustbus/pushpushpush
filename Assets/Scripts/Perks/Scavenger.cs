﻿using UnityEngine;

public class Scavenger : MonoBehaviour
{
    public static int pickupMultiplier = 1;
    bool activated = false;
    void Update()
    {
        if(gameObject.GetComponent<Scavenger>().enabled && !activated)
        {
            pickupMultiplier = 2;
            activated = true;
        }
    }
}
