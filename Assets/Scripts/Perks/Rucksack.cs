﻿using UnityEngine;

//More maximum ammo

public class Rucksack : MonoBehaviour
{
    public static bool activated = false;

    void Update()
    {
        if(gameObject.GetComponent<Rucksack>().enabled && !activated)
        {
            ShootingAR.maxMags *= 2;
            ShootingBolt.maxAmmo *= 2;
            ShootingFlak.maxAmmo *= 2;
            ShootingFlamer.maxAmmo *= 2;
            ShootingPlasma.maxMags *= 2;
            ShootingShotgun.maxMags *= 2;
            ShootingSSG.maxAmmo *= 2;
            ShootingStarter.maxMags *= 3;
            activated = true;
        }
    }
}
