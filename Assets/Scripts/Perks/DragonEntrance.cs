﻿using UnityEngine;
using System.Collections;
public class DragonEntrance : MonoBehaviour
{
    public static bool activated = false;
    void Update()
    {
        if(gameObject.GetComponent<DragonEntrance>().enabled && !activated)
        {
            activated = true;
        }
    }
    public static IEnumerator DragonEntranceGo()
    {
        ShootingFlamer.bulletForce = ShootingFlamer.bulletForce * 2f;
        ShootingFlamer.fireRate = ShootingFlamer.fireRate / 1.5f;
        ShootingSSG.fireRate = ShootingSSG.fireRate / 2f;
        ShootingSSG.reloadTime = ShootingSSG.reloadTime / 2f;
        ShootingFlak.fireRate = ShootingFlak.fireRate / 2f;
        ShootingFlak.reloadTime = ShootingFlak.reloadTime / 2f;
        ShootingBolt.fireRate = ShootingBolt.fireRate / 2f;
        ShootingBolt.reloadTime = ShootingBolt.reloadTime / 2f;
        ShootingAR.fireRate = ShootingAR.fireRate / 2f;
        ShootingPlasma.fireRate = ShootingPlasma.fireRate / 2f;
        ShootingShotgun.fireRate = ShootingShotgun.fireRate / 2f;
        ShootingStarter.fireRate = ShootingStarter.fireRate / 4f;
        ShootingStarter.reloadTime = ShootingStarter.reloadTime / 2f;
        ShootingPistol.fireRate = ShootingPistol.fireRate / 2f;
        yield return new WaitForSeconds(2.5f);
        ShootingFlamer.bulletForce = ShootingFlamer.bulletForce / 2f;
        ShootingFlamer.fireRate = ShootingFlamer.fireRate * 1.5f;
        ShootingSSG.fireRate = ShootingSSG.fireRate * 2f;
        ShootingSSG.reloadTime = ShootingSSG.reloadTime * 2f;
        ShootingFlak.fireRate = ShootingFlak.fireRate * 2f;
        ShootingFlak.reloadTime = ShootingFlak.reloadTime * 2f;
        ShootingBolt.fireRate = ShootingBolt.fireRate * 2f;
        ShootingBolt.reloadTime = ShootingBolt.reloadTime * 2f;
        ShootingAR.fireRate = ShootingAR.fireRate * 2f;
        ShootingPlasma.fireRate = ShootingPlasma.fireRate * 2f;
        ShootingShotgun.fireRate = ShootingShotgun.fireRate * 2f;
        ShootingStarter.fireRate = ShootingStarter.fireRate * 4f;
        ShootingStarter.reloadTime = ShootingStarter.reloadTime * 2f;
        ShootingPistol.fireRate = ShootingPistol.fireRate * 2f;
    }

}
