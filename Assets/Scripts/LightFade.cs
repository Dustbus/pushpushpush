﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFade : MonoBehaviour
{
    public Light light1;
    public float speed;
    // Update is called once per frame
    private void FixedUpdate()
    {
        light1.intensity = light1.intensity - speed;
    }
}
