﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneReset : MonoBehaviour   
{
    int enemyCount;
    int nextStageID;
    public static int levelsCompleted = 0;
    #region Elevator Animation Vars
    public Animator elevatorDoorAnimator;
    public Light pilotLight;
    #endregion

    void OnTriggerExit(Collider other)
    {
        Debug.Log("Elevator left");
        elevatorDoorAnimator.SetTrigger("ElevatorEmptyTrigger");
        StartCoroutine(ExecuteAfterTime(1));
        if(DragonEntrance.activated) StartCoroutine(DragonEntrance.DragonEntranceGo());
    }

    IEnumerator ExecuteAfterTime(float delay)
    {
        yield return new WaitForSeconds(delay);
        pilotLight.enabled = false;
    }
    void OnCollisionEnter(Collision colissionInfo)
    {
        if (colissionInfo.collider.tag == "Player")
        {
            enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
            if (enemyCount == 0f)
            {
                if(PlayerManager.levelUp)
                {
                    SceneManager.LoadScene()
                }
                else
                {
                    nextStageID = stageNumber();
                    SceneManager.LoadScene(nextStageID, LoadSceneMode.Single);
                    levelsCompleted++;
                    Debug.Log("Leves completed: " + levelsCompleted + ", Difficulty level: " + SpawnPointController.difficultyPoints);
                }
            }
        }
    }
    int stageNumber()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        do
        {
            nextStageID = Random.Range(2, SceneManager.sceneCountInBuildSettings);
        }
        while(nextStageID == currentScene.buildIndex);
        return nextStageID;
    }

    void Update()
    {
        //Debug.Log(GameObject.FindGameObjectsWithTag("Enemy").Length);
    }
}

