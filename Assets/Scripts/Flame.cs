﻿using System.Collections;
using UnityEngine;

public class Flame : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(DestroyFlame());
    }

    IEnumerator DestroyFlame()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}