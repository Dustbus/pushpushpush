﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    static public float speed = 650;
    public Rigidbody controller;
    public Transform player;
    Ray cameraRay;
    RaycastHit cameraRayHit;
    Vector3 movement;

    private void Update()
    {
        cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.z = Input.GetAxisRaw("Vertical");
        if (Physics.Raycast(cameraRay, out cameraRayHit))
        {
            if (cameraRayHit.transform)
            {
                Vector3 targetPosition = new Vector3(cameraRayHit.point.x, transform.position.y, cameraRayHit.point.z);
                player.LookAt(targetPosition);
            }
        }
    }
    private void FixedUpdate()
    {
        controller.AddForce(movement * speed * Time.fixedDeltaTime);

    }
}
