﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointController : MonoBehaviour
{
    public GameObject suicider;
    public GameObject gunner;
    public GameObject spawnPoint;
    public static int difficultyPoints;
    public int progressionStep = 5;
    private int spawnerCount;
    void Start()
    {
        difficultyPoints = 5 * (SceneReset.levelsCompleted / progressionStep + 1);
        spawnerCount = GameObject.FindGameObjectsWithTag("EnemySpawner").Length;
        Instantiate(gunner, spawnPoint.transform);
        Debug.Log("Spawn points in this level: " + spawnerCount);
    }

}
