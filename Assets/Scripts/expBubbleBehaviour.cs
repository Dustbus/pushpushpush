﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class expBubbleBehaviour : MonoBehaviour
{
    public Rigidbody bubbleRB;
    public GameObject bubbleObject;
    Transform target;
    NavMeshAgent navMeshAgent;
    public float magnetRange = 7f;
    void Start()
    {
        transform.rotation = Random.rotation;
        bubbleRB.AddForce(transform.forward * 2f, ForceMode.Impulse);
        target = PlayerManager.instance.player.transform;
        navMeshAgent = GetComponentInChildren<NavMeshAgent>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Destroy(bubbleObject);
            PlayerManager.playerEXP += 50;
            Debug.Log("EXP: " + PlayerManager.playerEXP);
        }   
    }

    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= magnetRange)
        {
            navMeshAgent.SetDestination(target.position);
        }
    }
}
