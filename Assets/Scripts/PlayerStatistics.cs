﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatistics : MonoBehaviour
{
    public Renderer playerModel;
    public Material playerMaterial;
    public Material dead;
    public bool gameOver = false;
    void OnTriggerEnter(Collider collider)
    {

        //Debug.Log("Collision detected");
        if (collider.tag == "ProjectileEnemy")
        {
            playerModel.material = dead;
            //Debug.Log(hp);
            StartCoroutine(ExecuteAfterTime(1));
            PlayerManager.playerHP -= 1;
        }

        if(PlayerManager.playerHP <= 0)
        {
            gameOver = true;
            //Debug.Log("You didn't make it, bruh");
        }

        IEnumerator ExecuteAfterTime(float delay)
        {
            yield return new WaitForSeconds(delay);

            playerModel.material = playerMaterial;
        }
    }
}
