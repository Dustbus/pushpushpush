﻿using UnityEngine;
using System.Collections;

public class Shrapnel : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(TurnCollisonOn());
    }

    IEnumerator TurnCollisonOn()
    {
        yield return new WaitForSeconds(0.05f);
        gameObject.GetComponent<SphereCollider>().enabled = true;
        yield return new WaitForSeconds(0.25f);
        Destroy(gameObject);
    }
}
