﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnPointController : MonoBehaviour
{
    //public GameObject weaponMain;
    //public GameObject weaponSecondary;
    //public GameObject weaponSpecial;
    public GameObject spawnPoint;
    public GameObject player;
    void Start()
    {
        Instantiate(player, spawnPoint.transform);
        //Instantiate(weaponMain, spawnPoint.transform);
        //Instantiate(weaponSecondary, spawnPoint.transform);
        //Instantiate(weaponSpecial, spawnPoint.transform);
    }
}
