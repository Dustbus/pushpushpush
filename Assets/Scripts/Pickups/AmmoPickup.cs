﻿using UnityEngine;
using System.Collections;

public class AmmoPickup : MonoBehaviour
{
    public Transform player;
    public GameObject icon;
    bool triggerCheck = false;

    private void Start()
    {
        StartCoroutine(FindPlayer());

    }
    private void OnTriggerEnter(Collider colider)
    {
        if (colider.tag == "Player")
        {
            triggerCheck = true;
            icon.GetComponent<MeshRenderer>().enabled = true;
            //Debug.Log(triggerCheck);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            triggerCheck = false;
            //Debug.Log(triggerCheck);
            icon.GetComponent<MeshRenderer>().enabled = false;
        }
    }


    private void Update()
    {
        if (triggerCheck)
        {
            if(Input.GetKeyDown("e"))
            {
                Pickup();
                Destroy(gameObject);
            }
        }
        Mathf.Clamp(ShootingFlamer.ammo, 0, ShootingFlamer.maxAmmo);
        Mathf.Clamp(ShootingSSG.ammo, 0, ShootingSSG.maxAmmo);
        Mathf.Clamp(ShootingShotgun.mags, 0, ShootingShotgun.maxMags);
        Mathf.Clamp(ShootingStarter.mags, 0, ShootingStarter.maxMags);
        Mathf.Clamp(ShootingPlasma.mags, 0, ShootingPlasma.maxMags);
        Mathf.Clamp(ShootingFlak.ammo, 0, ShootingFlak.maxAmmo);
        Mathf.Clamp(ShootingAR.mags, 0, ShootingAR.maxMags);
    }
    void Pickup()
    {
        for(int i = Scavenger.pickupMultiplier; i>0;i--)
        {
            ShootingFlamer.ammo += 100;
            ShootingSSG.ammo += 10;
            ShootingShotgun.mags += 2;
            ShootingBolt.ammo += 13;
            ShootingStarter.mags = ShootingStarter.maxMags;
            ShootingPlasma.mags += 1;
            ShootingFlak.ammo += 8;
            ShootingAR.mags += 2;
        }
    }
    IEnumerator FindPlayer()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }
}
