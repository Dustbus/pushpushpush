﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour
{
    public Transform player;
    public GameObject icon;
    bool triggerCheck = false;

    private void Start()
    {
        StartCoroutine(FindPlayer());

    }
    private void OnTriggerEnter(Collider colider)
    {
        if (colider.tag == "Player")
        {
            triggerCheck = true;
            icon.GetComponent<MeshRenderer>().enabled = true;
            //Debug.Log(triggerCheck);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            triggerCheck = false;
            //Debug.Log(triggerCheck);
            icon.GetComponent<MeshRenderer>().enabled = false;
        }
    }


    private void Update()
    {
        if (triggerCheck)
        {
            if(Input.GetKeyDown("e"))
            {
                Pickup();
                Destroy(gameObject);
            }
        }
    }
    void Pickup()
    {
        for(int i = Scavenger.pickupMultiplier; i>0;i--)
        {
            PlayerManager.playerHP += 1;
        }
    }
    IEnumerator FindPlayer()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }
}
