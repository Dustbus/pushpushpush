﻿using UnityEngine;
using System.Collections;

public class SecondarySwitch : MonoBehaviour
{
    public GameObject weapon;
    public GameObject lastWeapon;
    public Transform player;
    public GameObject icon;
    public float detectionSize;
    bool triggerCheck = false;

    private void Start()
    {
        StartCoroutine(FindPlayerAndGun());
    }
    IEnumerator FindPlayerAndGun()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] weapons = GameObject.FindGameObjectsWithTag("Weapon");
        foreach (GameObject foundWeapon in weapons)
        {
            if (foundWeapon != weapon)
            {
                lastWeapon = foundWeapon;
            }
        }
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!weapon.transform.IsChildOf(player) && other.tag == "Player")
        {
            triggerCheck = true;
            icon.GetComponent<MeshRenderer>().enabled = true;
            //Debug.Log(triggerCheck);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (weapon.transform.IsChildOf(player) || other.tag == "Player")
        {
            triggerCheck = false;
            //Debug.Log(triggerCheck);
            icon.GetComponent<MeshRenderer>().enabled = false;
        }
    }
    private void Update()
    {
        if(triggerCheck && !weapon.transform.IsChildOf(player))
        {
            if(Input.GetKeyDown("e"))
            {
                weapon.transform.SetParent(player);
                lastWeapon.transform.position = weapon.transform.position;
                lastWeapon.transform.rotation = weapon.transform.rotation;
                lastWeapon.transform.SetParent(null);
                weapon.transform.position = player.position;
                weapon.transform.rotation = player.rotation;
                icon.GetComponent<MeshRenderer>().enabled = false;
                Debug.Log(triggerCheck);
            }
        }
    }
}
