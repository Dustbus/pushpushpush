﻿using UnityEngine;
using System.Collections;

public class ShootingFlamer : MonoBehaviour
{
    public Transform weapon;
    public Transform player;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public static float bulletForce = 40f;
    public static float fireRate = 0.05f;
    public static float maxAmmo = 200;
    public static float ammo = 200;

    private float nextFire;
    private void Start()
    {
        StartCoroutine(FindPlayer());
    }
    void Update()
    {
        if (ammo > maxAmmo) ammo = maxAmmo;
        if (Input.GetButton("Fire1") && Time.time > nextFire && ammo > 0)
        {
            nextFire = Time.time + fireRate;
            if (weapon.IsChildOf(player))
                Shoot();
        }
    }
    IEnumerator FindPlayer()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }


    void Shoot()
    {
        Debug.Log(ammo);
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        bullet.transform.Rotate(Random.Range(-5, 5), Random.Range(-10, 10), Random.Range(-5, 5));
        rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);
        ammo--;
        Debug.Log("ammo:" + ammo);
    }
}
