﻿using UnityEngine;
using System.Collections;

public class ShootingShotgun : MonoBehaviour
{
    public Transform weapon;
    public Transform player;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public static float bulletForce = 50f;
    public static float fireRate = 0.5f;
    public static float reloadTime = 1.2f;
    public static int magSize = 6;
    public static int bulletsInMag = 6;
    public static int maxMags = 4;
    public static int mags = 4;

    private float nextFire;
    private void Start()
    {
        StartCoroutine(FindPlayer());
    }
    void Update()
    {
        if (bulletsInMag > magSize) bulletsInMag = magSize;
        if (mags > maxMags) mags = maxMags;
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire && bulletsInMag > 0)
        {
            nextFire = Time.time + fireRate;
            if (weapon.IsChildOf(player))
                Shoot();
        }
    }
    IEnumerator FindPlayer()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
        if (Input.GetKeyDown(KeyCode.R) && mags > 0)
        {
            StartCoroutine(Reload());
        }
    }

    IEnumerator Reload()
    {
        bulletsInMag = 0;
        Debug.Log("reloading");
        yield return new WaitForSeconds(reloadTime);
        mags--;
        bulletsInMag = magSize;
        Debug.Log("done");
    }

    void Shoot()
    {
        for (int i = 0; i < 6; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            bullet.transform.Rotate(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10));
            rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);
        }
        bulletsInMag--;
        Debug.Log("mags:" + mags);
        Debug.Log("left in mag:" + bulletsInMag);
    }
}
