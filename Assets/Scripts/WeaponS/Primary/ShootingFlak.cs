﻿using UnityEngine;
using System.Collections;

public class ShootingFlak : MonoBehaviour
{
    public Transform weapon;
    public Transform player;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public static float bulletForce = 50f;
    public static float fireRate = 2f;
    public static float reloadTime = 0.45f;
    public static int magSize = 4;
    public static int bulletsInMag = 4;
    public static int maxAmmo = 16;
    public static int ammo = 16;

    bool reloading = false;

    private float nextFire;
    private void Start()
    {
        StartCoroutine(FindPlayer());
    }
    void Update()
    {
        if (bulletsInMag > magSize) bulletsInMag = magSize;
        if (ammo > maxAmmo) ammo = maxAmmo;
        if (Input.GetButton("Fire1") && Time.time > nextFire && bulletsInMag > 0 && !reloading)
        {
            nextFire = Time.time + fireRate;
            if (weapon.IsChildOf(player))
                Shoot();
        }
        if (Input.GetKeyDown(KeyCode.R) && ammo > 0)
        {
            StartCoroutine(Reload());
        }
    }
    IEnumerator Reload()
    {
        reloading = true;
        while (ammo > 0 && bulletsInMag < magSize)
        {
            Debug.Log("reloading");
            yield return new WaitForSeconds(reloadTime);
            ammo--;
            bulletsInMag++;
            Debug.Log("done");
        }
        reloading = false;
    }
    IEnumerator FindPlayer()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }


    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
        bulletsInMag--;
        Debug.Log("mags:" + ammo);
        Debug.Log("left in mag:" + bulletsInMag);
    }
}
