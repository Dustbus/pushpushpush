﻿using UnityEngine;
using System.Collections;

public class ShootingSSG : MonoBehaviour
{
    public Transform weapon;
    public Transform player;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public static float bulletForce = 50f;
    public static float fireRate = 0.1f;
    public static float reloadTime = 0.35f;
    public static int magSize = 2;
    public static int bulletsInMag = 2;
    public static int maxAmmo = 20;
    public static int ammo = 20;

    bool reloading = false;

    private float nextFire;
    private void Start()
    {
        StartCoroutine(FindPlayer());
    }
    void Update()
    {
        if (bulletsInMag > magSize) bulletsInMag = magSize;
        if (ammo > maxAmmo) ammo = maxAmmo;
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire && bulletsInMag > 0 && !reloading)
        {
            nextFire = Time.time + fireRate;
            if (weapon.IsChildOf(player))
                Shoot();
        }
        if (Input.GetKeyDown(KeyCode.R) && ammo > 0)
        {
            StartCoroutine(Reload());
        }
    }
    IEnumerator FindPlayer()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }

    IEnumerator Reload()
    {
        reloading = true;
        while (ammo > 0 && bulletsInMag < magSize)
        {
            Debug.Log("reloading");
            yield return new WaitForSeconds(reloadTime);
            ammo--;
            bulletsInMag++;
            Debug.Log("done");
        }
        reloading = false;
    }

    void Shoot()
    {
        for (int i = 0; i < 9; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            bullet.transform.Rotate(Random.Range(-15, 15), Random.Range(-20, 20), Random.Range(-20, 20));
            rb.AddForce(bullet.transform.forward * bulletForce, ForceMode.Impulse);
        }
        bulletsInMag--;
        Debug.Log("ammo:" + ammo);
        Debug.Log("left in mag:" + bulletsInMag);
    }
}
