﻿using UnityEngine;
using System.Collections;

public class ShootingPlasma : MonoBehaviour
{
    public Transform weapon;
    public Transform player;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public static float bulletForce = 50f;
    public static float fireRate = 0.15f;
    public static float reloadTime = 1.5f;
    public static int magSize = 30;
    public static int bulletsInMag = 30;
    public static int maxMags = 2;
    public static int mags = 2;

    private float nextFire;
    private void Start()
    {
        StartCoroutine(FindPlayer());
    }
    void Update()
    {
        if (bulletsInMag > magSize) bulletsInMag = magSize;
        if (mags > maxMags) mags = maxMags;
        if (Input.GetButton("Fire1") && Time.time > nextFire && bulletsInMag > 0)
        {
            nextFire = Time.time + fireRate;
            if (weapon.IsChildOf(player))
                Shoot();
        }
        if (Input.GetKeyDown(KeyCode.R) && mags > 0)
        {
            StartCoroutine(Reload());
        }
    }
    IEnumerator Reload()
    {
        bulletsInMag = 0;
        Debug.Log("reloading");
        yield return new WaitForSeconds(reloadTime);
        mags--;
        bulletsInMag = magSize;
        Debug.Log("done");
    }

    IEnumerator FindPlayer()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }


    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
        bulletsInMag--;
        Debug.Log("mags:"+mags);
        Debug.Log("left in mag:"+bulletsInMag);
    }
}
