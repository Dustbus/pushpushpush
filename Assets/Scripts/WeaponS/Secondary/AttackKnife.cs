﻿using UnityEngine;

public class AttackKnife : MonoBehaviour
{
    public Transform weapon;
    public GameObject blade;
    public Transform player;
    public float fireRate = 0.4f;
    bool position = false;

    public Animator knifeAnimator;

    private float nextFire;
    private void Start()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }
    void Update()
    {
        if (Input.GetButton("Fire2") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            blade.GetComponent<BoxCollider>().enabled = true;
            blade.GetComponent<MeshRenderer>().enabled = true;
            if (weapon.IsChildOf(player))
            Attack();
        }
        if (Time.time > nextFire)
        {
            blade.GetComponent<MeshRenderer>().enabled = false;
            blade.GetComponent<BoxCollider>().enabled = false;
        }

    }

    void Attack()
    {
        if (position)
        {
            position = false;
            knifeAnimator.ResetTrigger("KnifeFalse");
            knifeAnimator.SetTrigger("KnifeTrue");
        }
        else
        {
            position = true;
            knifeAnimator.ResetTrigger("KnifeTrue");
            knifeAnimator.SetTrigger("KnifeFalse");
        }

    }
}
