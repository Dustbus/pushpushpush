﻿using UnityEngine;

public class ShootingPistol : MonoBehaviour
{
    public Transform weapon;
    public Transform player;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public static float bulletForce = 45f;
    public static float fireRate = 0.4f;

    private float nextFire;
    private void Start()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject foundPlayer in players)
        {
            player = foundPlayer.transform;
        }
    }
    void Update()
    {
        if (Input.GetButton("Fire2") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            if (weapon.IsChildOf(player))
            Shoot();
        }
    }

    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        bullet.transform.Rotate(Random.Range(-5, 5), Random.Range(-8, 8), Random.Range(-5, 5));
        rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
    }
}
