﻿using UnityEngine;

public class ElevatorController : MonoBehaviour
{
    public Renderer controlLight;
    public Light controlLightLight;
    public Light pilotLight;
    public Material on;
    public Material off;
    public Animator elevatorDoorAnimator;
    int enemyCount;

    void Start()
    {
        controlLightLight.enabled = false;
        pilotLight.enabled = true;
        controlLight.material = off;
    }

    void Update()
    {
        //Debug.Log("Enabled");
        enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        if (enemyCount == 0f)
        {
            //Debug.Log("Triggered");
            enabled = false;
            elevatorDoorAnimator.SetTrigger("ElevatorAnimTrigger");
            controlLightLight.enabled = true;
            pilotLight.enabled = true;
            controlLight.material = on;
        }
    }

}
