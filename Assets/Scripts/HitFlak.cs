﻿using UnityEngine;

public class HitFlak : MonoBehaviour
{
    public GameObject shrapnelPrefab;

    private void OnCollisionEnter(Collision collision)
    {
        for(float i = 0; i<10; i++)
        {
            GameObject shrapnel = Instantiate(shrapnelPrefab, gameObject.transform.position, gameObject.transform.rotation);
            Rigidbody rb = shrapnel.GetComponent<Rigidbody>();
            shrapnel.transform.Rotate(Random.Range(-20, 20), Random.Range(0, 359), Random.Range(-20, 20));
            rb.AddForce(shrapnel.transform.forward * 42, ForceMode.Impulse);
        }
        Destroy(gameObject);
    }
}
